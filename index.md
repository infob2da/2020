---
layout: home
title: Home
menu: Home
order: 1
---

<img src="assets/i/teaser.jpg">
<!-- <div class="credits"><a href="http://mbostock.github.io/d3/talk/20111116/bundle.html">Hierarchical edge bundling]</a> | <a href="http://hint.fm/wind/">Wind map</a> | <a href="http://www.nytimes.com/interactive/2012/10/15/us/politics/swing-history.html?_r=0">How states have shifted</a> </div> -->

Applied data analytics is a **multidisciplinary field** where you will learn insights needed to make sense of data, research, and observations from everyday life.

You will learn how to apply a **data-driven approach to problem-solving**, but will not only learn about tools, methods, and techniques, or the latest trends, but also more generic insights: why do certain approaches work, why the field is so popular, what common mistakes are made.

The lectures will provide the theoretical background of how a data analytics process should be performed.
Furthermore, we discuss an overview of **popular data analytics and visualization techniques** to help match techniques with information needs, including applications of text mining and data enrichment.

## Content

- Fundamental Data Mining Methods
- Data Preparation and Preprocessing
- Common Analysis Algorithms and Methods
- Principles of Information Visualization
- Human Perception and Visualization Design
- Data Visualization Techniques for Particular Data Types

The lecture is separated in two parts. The content of the first one are principal Data Mining methods whereas the main focus lies on Data Preprocessing, Cluster & Outlier Analysis, Classification and Association Rules. Subject of the second part are the basics of Information Visualization. Foundations of Human Perception and Design Decisions are followed by examples of visualizations of different data sources (Non-Spatial, Temporal, Geo-Spatial and 3D Spatial Data).

## Course Sessions

**Lectures:**  
Tuesday 11:00 - 12:45, Location: [Online](https://teams.microsoft.com/_#/school/files/General?threadId=19%3A04e76396759d4c2fabfb54e8e27ceb9d%40thread.tacv2&ctx=channel&context=Class%2520Materials&rootfolder=%252Fsites%252FINFOB2DA%252FClass%2520Materials)  
Thursday 13:15 - 15:00, Location: [Online](https://teams.microsoft.com/_#/school/files/General?threadId=19%3A04e76396759d4c2fabfb54e8e27ceb9d%40thread.tacv2&ctx=channel&context=Class%2520Materials&rootfolder=%252Fsites%252FINFOB2DA%252FClass%2520Materials)

<!-- Tuesday 11:00 - 12:45, Location: [BOL - 1.206](https://students.uu.nl/bolognalaan-101)
Thursday 11:00 - 12:45, Location: [Ruppert - 040](https://students.uu.nl/marinus-ruppertgebouw) -->

**Tutorials/Assigments/Labs (werkcollege):**  
_Due to the Covid-19 situation our lab locations will changing on the fly and will be announced (per group) on an individual basis through MS Teams_

_Group 1:_ Thursday 17:15 - 19:00, Location: [On SITE BBG - 209, all groups individually](https://students.uu.nl/buys-ballotgebouw)  
_Group 2:_ Thursday 17:15 - 19:00, Location: [On SITE BBG - 214, all groups individually](https://students.uu.nl/buys-ballotgebouw)  
_Group 3:_ Thursday 17:15 - 19:00, Location: [On SITE BBG - 001, all groups individually](https://students.uu.nl/buys-ballotgebouw)  
_Group 4:_ Thursday 17:15 - 19:00, Location: [On SITE BBG - 023, all groups individually](https://students.uu.nl/buys-ballotgebouw)  
_Group 5:_ Thursday 17:15 - 19:00, Location: [On SITE BBG - 161, all groups individually](https://students.uu.nl/buys-ballotgebouw)  
_Group 6:_ Thursday 17:15 - 19:00, Location: [On SITE BBG - 205, all groups individually](https://students.uu.nl/buys-ballotgebouw)  
_Group 7:_ Thursday 17:15 - 19:00, Location: [On SITE BBG - 315/317, all groups individually](https://students.uu.nl/buys-ballotgebouw)

**Office Hours:**  
Office hours are posted [here]({{ site.baseurl }}/schedule/#lab_oh_schedule).

**Lecture Resources:**  
Discussion forum on [MS Teams (Discussion Channel)](https://teams.microsoft.com/_#/school/files/General?threadId=19%3A04e76396759d4c2fabfb54e8e27ceb9d%40thread.tacv2&ctx=channel&context=Class%2520Materials&rootfolder=%252Fsites%252FINFOB2DA%252FClass%2520Materials)  
Materials and grades also on [MS Teams (General -> Files)](https://teams.microsoft.com/_#/school/files/General?threadId=19%3A04e76396759d4c2fabfb54e8e27ceb9d%40thread.tacv2&ctx=channel&context=General&rootfolder=%252Fsites%252FINFOB2DA%252FShared%2520Documents%252FGeneral)

**Workload:**

7.5 ECTS-Credits for lecture, tutorials, labs, and homeworks; Representing in total 210 hours, split into

- 50 hours course of study with attendance
- 160 hours of self-study time

## Instructor and Head TF

[Michael Behrisch](http://vig.science.uu.nl) (Instructor)  
Saba Gholizadeh, PhD Candidate, Software Technology (Head TF)

### Teaching Fellows

- _Group 1_ Saba Gholizadeh
- _Group 2_ Diede van der Hoorn
- _Group 3_ Hessel Laman
- _Group 4_ Anneloes Meijer
- _Group 5_ Simardeep Singh
- _Group 6_ Matthijs Ham
- _Group 7_ Nick Elbertse  
  (group allocation subject to change)

### COVID-19 Rules for this Class

We are following the [Utrecht University COVID-19 Rules](https://www.uu.nl/en/information-coronavirus).
Generally, we will keep the work as remote as possible, while still trying to foster community building aspect.

**Lectures** will be held remotely, but the _Labs/Werkcolleges_ are currently planned to be on-site.
_Please be aware that this information can change rapidly._

<!-- ### Previous Years

TBD -->

<!-- [2018 Fall Website](http://www.cs171.org/2018/)

[2017 Fall Website](http://www.cs171.org/2017/)

[2016 Fall Website](http://www.cs171.org/2016/)

[2016 Spring Website](http://www.cs171.org/2016_Spring/)

[2015 Website](http://www.cs171.org/2015/)
[2015 Video Archive](http://cm.dce.harvard.edu/2015/02/24028/publicationListing.shtml)

[2014 Website](http://www.cs171.org/2014/)
[2014 Video Archive](http://cm.dce.harvard.edu/2014/02/24028/publicationListing.shtml)

[2013 Video Archive](http://cm.dce.harvard.edu/2013/02/22872/publicationListing.shtml)

[2012 Video Archive](http://cm.dce.harvard.edu/2012/02/22872/publicationListing.shtml) -->
