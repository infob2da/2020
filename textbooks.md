- [The Art of Data Science](https://bookdown.org/rdpeng/artofdatascience/), A central reference is Peng and Matsui (2016), which is available as PDF, e-book, paperback, but you can also read the latest version online at https://bookdown.org/rdpeng/artofdatascience/. 
- [Visual Thinking for Design](http://www.amazon.com/Visual-Thinking-Kaufmann-Interactive-Technologies/dp/0123708966), Colin Ware, Morgan Kaufman (2008)
- [Interactive Data Visualization for the Web](http://shop.oreilly.com/product/0636920037316.do), Scott Murray, O’Reilly (2017) **Second edition! (The 2nd edition teaches D3 Version 4, which we will be using in this course!)**
- [Interactive Data Visualization: Foundations, Techniques, and Application](https://www.amazon.nl/Interactive-Data-Visualization-Foundations-Applications/dp/1568814739) Ward M. and Grinstein, G. and Keim D. A., 2010, A.K. Peters, Ltd, ISBN: 978-1-56881-473-5, [http//www.idvbook.com](http//www.idvbook.com)
<!-- [Free online version](http://chimera.labs.oreilly.com/books/1230000000345)-->

<!-- ## Recommended Textbooks -->

- [Visualization Analysis and Design](http://www.amazon.com/Visualization-Analysis-Design-Peters-Series/dp/1466508914), Tamara Munzner, CRC Press (2014)
- [The Functional Art: An introduction to information graphics and visualization](http://www.amazon.com/The-Functional-Art-introduction-visualization/dp/0321834739/), Alberto Cairo, New Riders (2012)
- [Design for Information](http://www.amazon.com/Design-Information-Isabel-Meirelles/dp/1592538061), Isabel Meirelles, Rockport (2013)

- Han J., Kamber M., Data Mining: Concepts and Techniques, 2006, Morgan Kaufmann Publishers, Second Edition
- Berthold M., Borgelt C., Höppner F., Klawonn F., Guide to Intelligent Data Analysis: How to Intelligently Make Sense of Real Data: Making Practical Sense of Real Data (Texts in Computer Science), 2010 Springer
- Hand D.J., Mannila H., Smyth P., Principles of Data Mining, 2001, MIT Press
- Spence R., Information Visualization, 2007, ACM Press Books, Second Edition
